#include <Bounce2.h>

#define IO_LED1   PB2
#define IO_LED2   PB3
#define IO_LED3   PB4
#define IO_PWM    PB1
#define IO_BUTTON PB0

Bounce debouncer = Bounce();
uint8_t pwm = 127;
uint8_t state = 0;

void setup() {
  debouncer.attach(IO_BUTTON, INPUT_PULLUP);
  debouncer.interval(25);
  pinMode(IO_LED1, OUTPUT);
  pinMode(IO_LED2, OUTPUT);
  pinMode(IO_LED3, OUTPUT);
  pinMode(IO_PWM, OUTPUT);
  digitalWrite(IO_LED1, HIGH);

  // Set Timer 0 prescaler to NONE
  TCCR0B = (1<<CS00); 
  // Set to 'Fast PWM' mode
  TCCR0A = (1<<WGM01)|(1<<WGM00)|(1<<COM0A1);
  // Clear OC0B output on compare match, upwards counting.
  TCCR0A |= (1 << COM0B1);
}

void loop() {
  debouncer.update();

  if (debouncer.fell()) {
    state = (state + 1) % 3;
    switch (state) {
      case 0: 
        pwm = 128; break;
      case 1:
        pwm = 192; break;
      case 2:
        pwm = 255; break;
    }
    digitalWrite(IO_LED2, state > 0);
    digitalWrite(IO_LED3, state > 1);
  }

  OCR0B = pwm;      // output PWM wave
}
